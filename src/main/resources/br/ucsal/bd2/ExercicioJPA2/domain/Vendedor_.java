package br.ucsal.bd2.ExercicioJPA2.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2018-05-27T14:29:37.388-0300")
@StaticMetamodel(Vendedor.class)
public class Vendedor_ extends Funcionario_ {
	public static volatile SingularAttribute<Vendedor, Double> percentualComissao;
	public static volatile SingularAttribute<Vendedor, SituacaoVendedorEnum> situacao;
	public static volatile ListAttribute<Vendedor, PessoaJuridica> clientes;
}
