package br.ucsal.bd2.ExercicioJPA2.domain;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2018-05-27T14:29:37.357-0300")
@StaticMetamodel(Funcionario.class)
public class Funcionario_ {
	public static volatile SingularAttribute<Funcionario, String> cpf;
	public static volatile SingularAttribute<Funcionario, String> nome;
	public static volatile SingularAttribute<Funcionario, String> rg;
	public static volatile SingularAttribute<Funcionario, String> rgOrgaoExpedidor;
	public static volatile SingularAttribute<Funcionario, String> rgUf;
	public static volatile ListAttribute<Funcionario, String> telefones;
	public static volatile SingularAttribute<Funcionario, Date> dataNascimento;
	public static volatile SingularAttribute<Funcionario, Endereco> endereco;
}
