package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2018-05-27T22:32:04.610-0300")
@StaticMetamodel(PessoaJuridica.class)
public class PessoaJuridica_ {
	public static volatile SingularAttribute<PessoaJuridica, String> cnpj;
	public static volatile SingularAttribute<PessoaJuridica, String> nome;
	public static volatile ListAttribute<PessoaJuridica, RamoAtividade> ramosAtividade;
	public static volatile SingularAttribute<PessoaJuridica, Double> faturamento;
	public static volatile ListAttribute<PessoaJuridica, Vendedor> vendedores;
}
