package model;

public enum SituacaoVendedorEnum {
	SUSPENSO("SPN"), 
	ATIVO("ATV");
	
	private String codigo;

	private SituacaoVendedorEnum(String codigo) {
		this.codigo = codigo;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public static SituacaoVendedorEnum valueOfCodigo(String codigo) {
		for (SituacaoVendedorEnum situacao : values()) {
			if (situacao.getCodigo().equalsIgnoreCase(codigo)) {
				return situacao;
			}
		}
		throw new IllegalArgumentException();
	}
	
}
