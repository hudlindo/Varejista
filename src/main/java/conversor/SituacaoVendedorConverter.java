package conversor;

import javax.persistence.AttributeConverter;

import model.SituacaoVendedorEnum;

public class SituacaoVendedorConverter implements AttributeConverter<SituacaoVendedorEnum, String> {

	@Override
	public SituacaoVendedorEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}
		return SituacaoVendedorEnum.valueOfCodigo(dbData);
	}
	
	@Override
	public String convertToDatabaseColumn(SituacaoVendedorEnum attribute) {
		if (attribute != null) {
			return attribute.getCodigo();
		}
		return null;
	}

}
