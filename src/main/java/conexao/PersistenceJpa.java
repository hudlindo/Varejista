package conexao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceJpa {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		try {

			emf = Persistence.createEntityManagerFactory("varejista");
			EntityManager em = emf.createEntityManager();

			em.getTransaction().begin();

			em.getTransaction().commit();

		} finally {

			if (emf != null) {
				emf.close();
			}
		}
	}
}
